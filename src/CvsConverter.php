<?php
namespace Ag\Twinstar;

use function PHPUnit\Framework\fileExists;

/**
 * CvsConverter
 */
class CvsConverter
{
    const REQUIRED_TWINSTAR_HEADERS = [
        "Posted Date",
        "Type",
        "Description",
        "Amount",
        "Account",
        "Notes",
        "Balance"
    ];

    const REQUIRED_YNAB_HEADERS = "Date,Payee,Memo,Outflow,Inflow";

    /** @var array */
    private $header = [];

    /** @var array */
    private $data = [];

    /**
     * Undocumented function
     *
     * @param mixed $cvsFilename
     */
    public function __construct($cvsFilename=null)
    {
        if (isset($cvsFilename)) {
            $this->fromCvsFile($cvsFilename);
        }
    }

    /**
     * fromCvsFile
     *
     * @param string $cvsFilename
     * @return bool
     * @throws CvsConverterException
     */
    public function fromCvsFile(string $cvsFilename): bool
    {
        // check if we have a file to work with
        if (empty($cvsFilename) || !file_exists($cvsFilename)) {
            throw new CvsConverterException(
                "CVS file does not exist: {$cvsFilename}",
                404
            );
        }

        $this->setData(file($cvsFilename));

        return true;
    }

    /**
     * asCSV
     *
     * @return array
     */
    public function asCSV(): array
    {
        $cvs = [];

        // Loop through file pointer and a line
        $data = $this->getData();
        $headers = array_shift($data);

        // write headers
        $cvs[] = sprintf(
            '"%s","%s","%s","%s","%s"'.PHP_EOL,
            $headers[0],
            $headers[1],
            $headers[2],
            $headers[3],
            $headers[4],
        );

        // write data
        foreach ($data as $fields) {
            if (gettype($fields) === 'array') {
                $line = sprintf(
                    '"%s","%s","%s","%s","%s"'.PHP_EOL,
                    $fields['Date'],
                    $fields['Payee'],
                    $fields['Memo'],
                    $fields['Outflow'],
                    $fields['Inflow'],
                );
                $cvs[] = $line;
            }
        }

        return $cvs;
    }


    /**
     * getData
     *
     * @return array
     */
    public function getData(): array
    {
        $ynabData = [];
        $ynabData[] = explode(',', self::REQUIRED_YNAB_HEADERS);

        foreach($this->data as $item) {
            $data = [];
            $dateInput = new \DateTime($item['Posted Date']);
            $data['Date'] = date_format($dateInput, "Y-m-d");
            $data['Payee'] = $item['Description'];
            $data['Memo'] = $item['Notes'];
            if (floatval($item['Amount']) < 0) {
                $data['Outflow'] = abs($item['Amount']);
                $data['Inflow'] = "";
            } else {
                $data['Outflow'] = "";
                $data['Inflow'] = abs($item['Amount']);
            }

            //$ynabData[] = implode (",", $data);
            $ynabData[] = $data;
        }

        return $ynabData;
    }

    /**
     * setData
     *
     * @param array $data
     * @throws CvsConverterException
     */
    public function setData(array $data=[]): void
    {
        // read the CSV file into an array
        $this->data = array_map('str_getcsv', $data);

        // determine array key names
        $this->header = array_shift($this->data);

        // test for requred headers
        $diffHeaders = array_diff($this->header, self::REQUIRED_TWINSTAR_HEADERS);
        if (count($diffHeaders) != 0) {
            throw new CvsConverterException(
                'header values do not match expected values',
                500
            );
        }

        // munge CSV array into associative array
        array_walk($this->data, [$this, '_combineArray'], $this->header);
    }

    /**
     * _combineArray
     *
     * @param array $row
     * @param string $key
     * @param array $header
     * @return mixed
     */
    private function _combineArray(array &$row, string $key, array $header): void
    {
        $row = array_combine($header, $row);
    }
}
