<?php declare(strict_types=1);

use Ag\Twinstar\CvsConverter;
use Ag\Twinstar\CvsConverterException;
use PHPUnit\Framework\TestCase;

final class CvsConverterTest extends TestCase
{
    /**
     * testCanBeCreatedFromValidCvsFile
     *
     * @return void
     */
    public function testCanBeCreatedFromValidCvsFile(): void
    {
        $this->assertInstanceOf(
            CvsConverter::class,
            new CvsConverter(),
            "Not the class type expectred. Expecting CvsConverter."
        );
    }

    /**
     * testCannotBeCreatedFromInvalidCvsFile
     *
     * @return void
     */
    public function testCannotBeCreatedFromNonExistantCvsFile(): void
    {
        $this->expectException(Ag\Twinstar\CvsConverterException::class);

        $cvsConverter = new CvsConverter('invalid');
    }

    public function testCanBeCreatedFromCvsFile(): void
    {
        $list = [
            ["Posted Date","Type","Description","Amount","Account","Notes","Balance"],
            ["2021-10-03T07:00:00+00:00","withdrawal","TIAA BANK",-450.00,"488209=90","",20484.06],
            ["2021-10-03T07:00:00+00:00","withdrawal","MUD BAY LACEY CROSSROAD",-108.82,"488209=90","",20934.06],
            ["2021-10-02T07:00:00+00:00","withdrawal","Subway",-12.64,"488209=90","",21042.88],
        ];
        
        $fileName = dirname(__FILE__)."/contacts.csv";
        $file = fopen($fileName, "w");
        foreach ($list as $line) {
            fputcsv($file, $line);
        }
        fclose($file);

        $cvsConverter = new CvsConverter();
        $this->assertTrue(
            $cvsConverter->fromCvsFile($fileName),
            'can not read the input file'
        );

        unlink($fileName);
    }

    public function testCanBeCreatedFromSetter(): void
    {
        $list = [
            "Posted Date,Type,Description,Amount,Account,Notes,Balance",
            "2021-10-03T07:00:00+00:00,withdrawal,TIAA BANK,-450.00,488209=90,,20484.06",
            "2021-10-03T07:00:00+00:00,withdrawal,MUD BAY LACEY CROSSROAD,-108.82,488209=90,,20934.06",
            "2021-10-02T07:00:00+00:00,withdrawal,Subway,-12.64,488209=90,,21042.88",
        ];

        $cvsConverter = new CvsConverter();
        $cvsConverter->setData($list);

        $this->assertIsArray(
            $cvsConverter->getData(),
            'can not map the input list'
        );
    }
}
