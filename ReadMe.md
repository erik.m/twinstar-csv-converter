TwinStar CSV Converter
====================

![Shields](https://img.shields.io/badge/PHP-TwinStar%20CSV%20Converter-green)
![Shields](https://img.shields.io/badge/License-MIT-teal)

Simple PHP script to transform a TwinStar CSV download into a YNAB CSV upload.
