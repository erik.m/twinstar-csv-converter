#!/usr/bin/env php
<?php
namespace Ag\Twinstar;

require_once dirname(__FILE__)."/vendor/autoload.php";

// setup default output filename
$today = date("Y-m-d");
$outFileName = "cvs-converter.{$today}.csv";

// get command line options
$opts = getopt("i:o:", ['input:','output:']);
foreach($opts as $opt=> $value) {
    switch ($opt) {
        case 'i':
        case 'input':
            $fileName = $value;
            if (!file_exists(realpath($fileName))) {
                echo "Error: file '{$fileName}' does not exist." . PHP_EOL;
                exit;
            }

        case 'o':
        case 'output':
            $outFileName = $value;
    }
}

// test for required parameters
if (!isset($fileName) || !isset($outFileName)) {
    echo "Error: missing required input and/or output file names." . PHP_EOL;
    exit;
}
echo "input: {$fileName}, output: {$outFileName}" . PHP_EOL;

// Import TwinStar data
$cvsConverter = new CvsConverter($fileName);

// Open a file in write mode ('w')
if ($fp = fopen($outFileName, 'w')) {
    foreach($cvsConverter->asCSV() as $line) {
        fwrite($fp, $line);
        // echo $line;
    }
    fclose($fp);
    echo "Success: created {$outFileName}!" . PHP_EOL;
} else {
    echo "Error: could not open {$outFileName} for writing." . PHP_EOL;
}
